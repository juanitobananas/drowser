package com.jarsilio.android.drowser.prefs

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.Preference
import androidx.preference.PreferenceManager
import androidx.preference.SwitchPreferenceCompat
import com.jarsilio.android.drowser.PreferencesActivity
import com.jarsilio.android.drowser.R
import com.jarsilio.android.drowser.models.SingletonHolder
import com.jarsilio.android.drowser.services.Scheduler

class Prefs private constructor(private val context: Context) {
    val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    var fragment: PreferencesActivity.SettingsFragment? = null

    // User prefs
    val isEnabledKey: String = context.getString(R.string.pref_enabled_key)
    val showNotificationKey: String = context.getString(R.string.pref_notification_key)
    val drowserForegroundAppKey: String = context.getString(R.string.pref_drowse_foreground_app_key)
    val drowserDelayKey: String = context.getString(R.string.pref_drowse_delay_key)
    val showSystemAppsKey: String = context.getString(R.string.pref_drowse_show_system_apps_key)
    val showDisabledAppsKey: String = context.getString(R.string.pref_drowse_show_disabled_apps_key)

    var isEnabled: Boolean
        get() = prefs.getBoolean(isEnabledKey, true)
        set(value) = setBooleanPreference(isEnabledKey, value)

    var showNotification: Boolean
        get() = prefs.getBoolean(showNotificationKey, true)
        set(value) = setBooleanPreference(showNotificationKey, value)

    var drowseForegroundApp: Boolean
        get() = prefs.getBoolean(drowserForegroundAppKey, true)
        set(value) = setBooleanPreference(drowserForegroundAppKey, value)

    var drowseDelay: Long
        get() = prefs.getString(drowserDelayKey, "0")!!.toLong()
        set(value) = prefs.edit().putString(drowserDelayKey, value.toString()).apply()

    var showSystemApps: Boolean
        get() = prefs.getBoolean(showSystemAppsKey, false)
        set(value) = setBooleanPreference(showSystemAppsKey, value)

    var showDisabledApps: Boolean
        get() = prefs.getBoolean(showDisabledAppsKey, false)
        set(value) = setBooleanPreference(showDisabledAppsKey, value)

    // Automagic prefs: these are not real prefs. They are just set by the app to remember stuff
    private val requestRootAccessKey: String = context.getString(R.string.pref_request_root_access_key)
    private val disableUntilKey: String = context.getString(R.string.pref_disable_until_key)
    private val lastDisableUntilUserChoiceKey: String = context.getString(R.string.pref_last_disable_until_user_choice_key)

    var requestRootAccess: Boolean
        get() = prefs.getBoolean(requestRootAccessKey, true)
        set(value) = prefs.edit().putBoolean(requestRootAccessKey, value).apply()

    var disableUntil: Long
        get() = prefs.getLong(disableUntilKey, 0)
        set(value) {
            prefs.edit().putLong(disableUntilKey, value).apply()
            Scheduler.scheduleAlarm(context, value)
        }

    var lastDisableUntilUserChoice: Int
        get() = prefs.getInt(lastDisableUntilUserChoiceKey, 2)
        set(value) = prefs.edit().putInt(lastDisableUntilUserChoiceKey, value).apply()

    private fun setBooleanPreference(
        key: String,
        value: Boolean,
    ) {
        // This changes the GUI, but it needs the PreferencesActivity to have started
        val preference = fragment?.findPreference<Preference>(key)
        if (preference is SwitchPreferenceCompat) {
            preference.isChecked = value
        }
        // This doesn't change the GUI
        prefs.edit().putBoolean(key, value).apply()
    }

    companion object : SingletonHolder<Prefs, Context>(::Prefs)
}
