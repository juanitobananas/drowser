New in 1.0.5

★ Fix crash in Android 14

New in 1.0.4

★ Upgrade a bunch of dependencies.
★ Do some Android 13 related changes (like requestion POST_NOTIFICATIONS permission).

New in 1.0.3

★ Show notification if root is not granted.

New in 1.0.2

★ Upgrade a bunch of dependencies.
